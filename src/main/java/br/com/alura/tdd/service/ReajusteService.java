package br.com.alura.tdd.service;

import br.com.alura.tdd.modelo.Funcionario;
import br.com.alura.tdd.service.reajustes.IReajuste;

public class ReajusteService  {

    public void concederReajuste(Funcionario funcionario, IReajuste reajuste) {
        funcionario.reajustarSalario(reajuste.reajusteSalario(funcionario));
    }

}
