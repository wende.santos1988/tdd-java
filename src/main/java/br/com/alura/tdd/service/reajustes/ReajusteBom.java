package br.com.alura.tdd.service.reajustes;

import br.com.alura.tdd.modelo.Funcionario;

import java.math.BigDecimal;

public class ReajusteBom implements IReajuste {
    @Override
    public BigDecimal reajusteSalario(Funcionario funcionario) {
        return funcionario.getSalario().multiply(new BigDecimal("0.15"));
    }
}
