package br.com.alura.tdd.service.reajustes;

import br.com.alura.tdd.modelo.Funcionario;

import java.math.BigDecimal;

public interface IReajuste {

    BigDecimal reajusteSalario(Funcionario funcionario);

}
