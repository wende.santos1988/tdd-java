package br.com.alura.tdd.modelo;

import java.math.BigDecimal;
// Enum criado para alternativa ao strategy que foi implementado usando uma interface
public enum Desempenho {
    A_DESEJAR {
        @Override
        public BigDecimal percentualReajuste() {
            return new BigDecimal(" 0.03");
        }
    },
    BOM {
        @Override
        public BigDecimal percentualReajuste() {
            return new BigDecimal(" 0.15");
        }
    },
    OTIMO {
        @Override
        public BigDecimal percentualReajuste() {
            return new BigDecimal(" 0.20");
        }
    };

    public abstract BigDecimal percentualReajuste();
}
