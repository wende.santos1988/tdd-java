package br.com.alura.testes_basico;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void deveriaSomarDoisNumeros(){
        Calculadora calculadora = new Calculadora();
        int soma = calculadora.somar(7, 3);
        assertEquals(10, soma);
    }

}
