package br.com.alura.tdd.service;

import br.com.alura.tdd.modelo.Funcionario;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class BonusServiceTest {

    @Test
    void bonusDeveriaSerZeroParaFuncionarioComSalarioMuitoAlto() {

        BonusService bonusService = new BonusService();

        assertThrows(IllegalArgumentException.class,
                () -> bonusService.calcularBonus(
                        new Funcionario("Wendel Santos", LocalDate.now(), new BigDecimal("15000"))));

//        try {
//            bonusService.calcularBonus(
//                new Funcionario("Wendel Santos", LocalDate.now(), new BigDecimal("15000")));
//            fail("Exception não ocorreu");
//        } catch (Exception e) {
//            assertEquals("Funcionário com salario mais que R$10.000,00 não pode receber bonus.", e.getMessage());
//        }
    }

    @Test
    void bonusDeveriaSer10PorCendoDoSalarioParaFuncionario() {

        BonusService bonusService = new BonusService();
        BigDecimal bonus = bonusService.calcularBonus(
                new Funcionario("Wendel Santos", LocalDate.now(), new BigDecimal("2500")));

        assertEquals(new BigDecimal("250.00"), bonus);
    }

    @Test
    void bonusDeveriaSer10PorCentoParaSalarioDeExatamente10Mil() {

        BonusService bonusService = new BonusService();
        BigDecimal bonus = bonusService.calcularBonus(
                new Funcionario("Wendel Santos", LocalDate.now(), new BigDecimal("10000")));

        assertEquals(new BigDecimal("1000.00"), bonus);
    }
}
