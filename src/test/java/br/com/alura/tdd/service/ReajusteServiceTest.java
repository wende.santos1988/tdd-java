package br.com.alura.tdd.service;

import br.com.alura.tdd.modelo.Funcionario;
import br.com.alura.tdd.service.reajustes.ReajusteADesejar;
import br.com.alura.tdd.service.reajustes.ReajusteBom;
import br.com.alura.tdd.service.reajustes.ReajusteOtimo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReajusteServiceTest {

    private ReajusteService service;
    private Funcionario funcionario;

    @BeforeEach
    public void inicializar(){
        this.service = new ReajusteService();
        this.funcionario = new Funcionario("Wendel", LocalDate.now(), new BigDecimal("1000.00"));
    }

    @Test
    public void reajusteDeveriaSerDe3PorCentoQuandoDesempenhoForADesejar(){
        service.concederReajuste(funcionario, new ReajusteADesejar());
        assertEquals(new BigDecimal("1030.00"), funcionario.getSalario());
    }

    @Test
    public void reajusteDeveriaSerDe3PorCentoQuandoDesempenhoForBom(){
        service.concederReajuste(funcionario, new ReajusteBom());
        assertEquals(new BigDecimal("1150.00"), funcionario.getSalario());
    }

    @Test
    public void reajusteDeveriaSerDe3PorCentoQuandoDesempenhoForOtimo(){
        service.concederReajuste(funcionario, new ReajusteOtimo());
        assertEquals(new BigDecimal("1200.00"), funcionario.getSalario());
    }

}
